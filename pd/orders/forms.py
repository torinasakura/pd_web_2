# coding=utf-8
import datetime

from django import forms
from django.db.models.deletion import ProtectedError
from django.db.models.query_utils import Q
from django.forms.models import inlineformset_factory, BaseInlineFormSet
from django.utils.translation import ugettext as _

from burials.models import Burial
from geo.forms import LocationForm
from orders.models import Product, Order, OrderItem, CatafalqueData, CoffinData, AddInfoData
from burials.forms import EMPTY
from pd.forms import ChildrenJSONMixin
from persons.forms import AlivePersonForm, PersonIDForm
from persons.models import AlivePerson, PersonID, SafeDeleteMixin
from users.models import Org, Profile

class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        exclude = ['loru', ]

class OrderForm(ChildrenJSONMixin, SafeDeleteMixin, forms.ModelForm):

    class Meta:
        model = Order
        exclude = ['loru', 'applicant', ]

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(OrderForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder.insert(0, self.fields.keyOrder.pop(-1))

        remove_opf_empty = request.user.profile.org.opf_order_customer_mandatory
        if self.instance.pk:
            if self.instance.applicant:
                opf_initial = Org.OPF_PERSON
            elif self.instance.applicant_organization:
                opf_initial = Org.OPF_ORG
            else:
                opf_initial = Org.OPF_EMPTY
                remove_opf_empty = False
        else:
            if request.user.profile.org.opf_order_customer_mandatory:
                opf_initial = request.user.profile.org.opf_order
            else:
                opf_initial = Org.OPF_EMPTY
            self.initial.update({'dt': datetime.date.today()})
            
        choices=list(Org.OPF_CHOICES)
        if remove_opf_empty:
            for i, choice in enumerate(choices):
                if choice[0] == Org.OPF_EMPTY:
                    choices.pop(i)
                    break
        self.fields['opf'] = forms.ChoiceField(label='', widget=forms.RadioSelect,
                                               choices=choices, initial = opf_initial)

        self.fields['payment'].widget = forms.RadioSelect(choices=Order.PAYMENT_CHOICES)

        self.fields['applicant_organization'].queryset = Org.objects.all()
        self.fields['applicant_organization'].inactive_queryset = \
            Org.objects.filter(Q(profile=None) | ~Q(profile__user__is_active=True)).distinct()
        self.fields['agent'].queryset = Profile.objects.filter(is_agent=True).select_related('user')
        self.fields['dover'].queryset = self.fields['dover'].queryset.select_related('agent', 'agent__user')

        # Отсутствие выбора будет в выпадающем списке не "---", а ""
        self.fields['applicant_organization'].empty_label = ''
        
        self.forms = self.construct_forms()

    def is_valid(self):
        return super(OrderForm, self).is_valid() and all([f.is_valid() for f in self.forms])

    def clean(self):
        if self.cleaned_data.get('opf') == Org.OPF_ORG:
            if not (self.cleaned_data.get('agent_director') or \
                    self.cleaned_data.get('agent') and self.cleaned_data.get('dover')
                   ):
                raise forms.ValidationError(_(u'Нет данных об агенте и/или доверенности для заявителя-ЮЛ. Изменения не сохранены'))
            dover = self.cleaned_data.get('dover')
            if dover and not dover.begin <= self.cleaned_data.get('dt') <= dover.end:
                    raise forms.ValidationError(_(u'Дата заказа не соответствует сроку действия доверенности. Изменения не сохранены'))
        elif self.cleaned_data.get('opf') == Org.OPF_PERSON:
            if not self.applicant_form.is_valid_data():
                raise forms.ValidationError(_(u"Нужно указать Заявителя-ФЛ"))
        return self.cleaned_data
            
    def construct_forms(self):
        data = self.data or None
        applicant = self.instance and self.instance.applicant
        self.applicant_form = AlivePersonForm(data=data, prefix='applicant', instance=applicant)
        applicant_addr = applicant and applicant.address
        self.applicant_address_form = LocationForm(data=data, prefix='applicant-address', instance=applicant_addr)
        try:
            applicant_id = self.instance and self.instance.applicant and self.instance.applicant.personid
        except PersonID.DoesNotExist:
            applicant_id = None
        self.applicant_id_form = PersonIDForm(data=data, prefix='applicant-pid', instance=applicant_id)

        return [self.applicant_form, self.applicant_address_form, self.applicant_id_form]

    def save(self, commit=True, *args, **kwargs):
        self.instance = super(OrderForm, self).save(commit=False)
        
        if self.cleaned_data.get('opf') == Org.OPF_PERSON:
            if self.applicant_form.is_valid_data():
                applicant = self.applicant_form.save(commit=False)
                if self.applicant_address_form.is_valid_data():
                    applicant.address = self.applicant_address_form.save()
                applicant.save()

                if self.applicant_id_form.is_valid_data():
                    pid = self.applicant_id_form.save(commit=False)
                    pid.person = applicant
                    pid.save()
                self.instance.applicant = applicant
            else:
                self.instance.applicant = None
            self.instance.applicant_organization = None
        elif self.cleaned_data.get('opf') == Org.OPF_ORG:
            self.safe_delete('applicant', self.instance)
            if self.cleaned_data.get('agent_director'):
                self.instance.agent = None
                self.instance.dover = None
        elif self.cleaned_data.get('opf') == Org.OPF_EMPTY:
            self.safe_delete('applicant', self.instance)
            self.instance.applicant_organization = None
            self.instance.agent_director = False
            self.instance.agent = None
            self.instance.dover = None

        self.instance.save()

        return self.instance

class OrderItemForm(forms.ModelForm):

    class Meta:
        model = OrderItem
        fields = ('product', 'cost', 'quantity',)
        
    def __init__(self, *args, **kwargs):
        super(OrderItemForm, self).__init__(*args, **kwargs)
        self.fields['cost'].required = False

    #def clean(self):
        #p = self.cleaned_data['product']
        #if p and p.ptype and p.ptype != Product.PRODUCT_CATAFALQUE:
            #self.cleaned_data['quantity'] = 1
        #return self.cleaned_data
        
    def clean(self):
        for f in self.formset:
            if (f is not self) and f['product'].value() == self['product'].value():
                raise forms.ValidationError(_(u'Нельзя добавить два или более одинаковых товаров/услуг'))
        return self.cleaned_data

class BaseOrderItemFormset(BaseInlineFormSet):
    def __init__(self, request, *args, **kwargs):
        super(BaseOrderItemFormset, self).__init__(*args, **kwargs)
        for f in self.forms:
            f.fields['product'].queryset = Product.objects.filter(loru=request.user.profile.org)
            f.formset = self
            
    #def get_same_product(self, form):
        #p = form.cleaned_data['product']

        #q = Q(product=p)
        #if p.ptype:
            #q |= Q(product__ptype=p.ptype)
        #same_product = OrderItem.objects.filter(q, order=self.instance)

        #if same_product.exists():
            #if p.ptype:
                #if p.ptype == Product.PRODUCT_CATAFALQUE:
                    #same_product.update(quantity=form.cleaned_data['quantity'])
                #else:
                    #same_product.update(quantity=1)
            #try:
                #return same_product[0]
            #except IndexError:
                #pass

    #def save_existing(self, form, instance, commit=True):
        #return self.get_same_product(form) or super(BaseOrderItemFormset, self).save_existing(form, instance, commit)
    
    #def save_new(self, form, commit=True):
        #return self.get_same_product(form) or super(BaseOrderItemFormset, self).save_new(form, commit)

OrderItemFormset = inlineformset_factory(Order, OrderItem, form=OrderItemForm, formset=BaseOrderItemFormset, extra=1)

class CatafalqueForm(forms.ModelForm):
    class Meta:
        model = CatafalqueData

    def __init__(self, *args, **kwargs):
        super(CatafalqueForm, self).__init__(*args, **kwargs)
        self.fields['start_place'].widget = forms.TextInput()

class AddInfoForm(forms.ModelForm):
    class Meta:
        model = AddInfoData

class CoffinForm(forms.ModelForm):
    size = forms.CharField(label=_(u'Размер'))

    class Meta:
        model = CoffinData

class OrderSearchForm(forms.Form):
    """
    Форма поиска заказов
    """

    PAGE_CHOICES = (
        (10, 10),
        (25, 25),
        (50, 50),
        (100, 100),
    )

    fio = forms.CharField(required=False, max_length=100, label=_(u"ФИО"))
    no_last_name = forms.BooleanField(required=False, initial=False, label=_(u"Неизв."))
    birth_date_from = forms.DateField(required=False, label=_(u"Дата рожд. с"))
    birth_date_to = forms.DateField(required=False, label=_(u"по"))
    death_date_from = forms.DateField(required=False, label=_(u"Дата смерти с"))
    death_date_to = forms.DateField(required=False, label=_(u"по"))
    burial_date_from = forms.DateField(required=False, label=_(u"Дата захор. с"))
    burial_date_to = forms.DateField(required=False, label=_(u"по"))
    account_number_from = forms.IntegerField(required=False, label=_(u"Номер Заказа с"))
    account_number_to = forms.IntegerField(required=False, label=_(u"по"))
    applicant_org = forms.CharField(required=False, max_length=60, label=_(u"Заказчик-ЮЛ"))
    applicant_person = forms.CharField(required=False, max_length=40, label=_(u"Заказчик-ФЛ"))
    responsible = forms.CharField(required=False, max_length=40, label=_(u"Ответственный"))
    cemetery = forms.CharField(required=False, label=_(u"Кладбища"))
    area = forms.CharField(required=False, label=_(u"Участок"))
    row = forms.CharField(required=False, label=_(u"Ряд"))
    place = forms.CharField(required=False, label=_(u"Место"))
    no_responsible = forms.BooleanField(required=False, initial=False, label=_(u"Без отв."))
    status = forms.TypedChoiceField(required=False, label=_(u"Статус"), choices=EMPTY + Burial.STATUS_CHOICES)
    annulated = forms.BooleanField(required=False, initial=False, label=_(u"Аннулированы"))
    order_num_from = forms.IntegerField(required=False, label=_(u"Номер Заказа с"))
    order_num_to = forms.IntegerField(required=False, label=_(u"по"))
    order_cost_from = forms.IntegerField(required=False, label=_(u"Стоимость с"))
    order_cost_to = forms.IntegerField(required=False, label=_(u"по"))
    per_page = forms.ChoiceField(label=_(u"На странице"), choices=PAGE_CHOICES, initial=25, required=False)
    burial_num_from = forms.IntegerField(required=False, label=_(u"Номер Захоронения с"))
    burial_num_to = forms.IntegerField(required=False, label=_(u"по"))
    reg_number_from = forms.IntegerField(required=False, label=_(u"Рег № с"))
    reg_number_to = forms.IntegerField(required=False, label=_(u" по "))
    burial_container = forms.TypedChoiceField(required=False, label=_(u"Тип захоронения"), choices=EMPTY + Burial.BURIAL_CONTAINERS)

class OrderBurialForm(forms.ModelForm):
    """
    Форма создания или привязки захоронения к заказу
    """
    class Meta:
        model = Order
        fields = ()

    NB_CHOICES = (('new', _(u'Новое захоронение')), ('bind', _(u'Существующее')))
    
    nb_choice = forms.ChoiceField(label='', choices=NB_CHOICES, widget=forms.RadioSelect, initial='new')
    nb_burial = forms.IntegerField(required=False, label=_(u"Номер захоронения"))
    
    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(OrderBurialForm, self).__init__(*args, **kwargs)

    def clean(self):
        cd = self.cleaned_data
        if self.is_valid():
            if cd['nb_choice'] == 'bind':
                if not cd['nb_burial']:
                    raise forms.ValidationError(_(u'Задайте номер захоронения'))
                try:
                    burial = Burial.objects.get(pk=cd['nb_burial'])
                    if burial.is_annulated() and \
                       burial.is_full() and burial.loru and burial.loru == self.request.user.profile.org:
                        # своё аннулированное. Чужие проверяются ниже
                        raise forms.ValidationError(_(u'Анулированное захоронение нельзя прикрепить к заказу'))
                    elif not burial.can_bind_to_order(self.request.user.profile.org):
                        raise forms.ValidationError(_(u'Это захоронение недоступно вашей организации'))
                    self.instance.burial = burial
                    self.instance.save()
                except Burial.DoesNotExist:
                    raise forms.ValidationError(_(u'Нет такого захоронения'))
        return cd

