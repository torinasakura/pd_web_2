from django.contrib import admin

from users.models import Profile, Org, ProfileLORU, Dover


class AgentDoverInline(admin.TabularInline):
    model = Dover
    can_delete = False

class ProfileLORUInline(admin.TabularInline):
    model = ProfileLORU
    fk_name = 'ugh'

class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'full_name', 'org', 'is_agent', ]
    list_filter = ['is_agent',]
    inlines = [AgentDoverInline, ]
    search_fields = ['user__username', 'user_last_name', 'org__name']

admin.site.register(Profile, ProfileAdmin)

class OrgAdmin(admin.ModelAdmin):
    inlines = [ProfileLORUInline, ]
    list_display = ['name', 'type']
    readonly_fields =  ['off_address', ]
    search_fields = ['name']

admin.site.register(Org, OrgAdmin)
