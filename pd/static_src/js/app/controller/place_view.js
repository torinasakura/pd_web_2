﻿app.controller('PlaceViewCtrl', function PlaceViewCtrl($scope, $routeParams, $location, 
	Place, Cemetery, Grave, GravePhoto, Burial, Area, AlivePerson, Phone, Log, ymapData, $dialog) {
	"use strict";

	//Constants
	//todo put all constants in constant provider
	$scope.version_str = version_str;
	$scope.BURIAL_CONTAINERS = BURIAL_CONTAINERS;
	$scope.BURIAL_TYPES = BURIAL_TYPES;
	$scope.STATUS_CHOICES = STATUS_CHOICES;
	$scope.BURIAL_STATUS_EXHUMATED = BURIAL_STATUS_EXHUMATED;
    $scope.grave_page = 1;
    $scope.log_page = 1;
    $scope.loading = false;

    
    var item_params;
	//setup
	$scope.updateMap = function() {
		if($scope.item){
		  ymapData.markers = [{
				point: [
				        geo.getLat($scope.item.lat),
				        geo.getLng($scope.item.lng)
				        ],
				caption: 'Место: "{0}"'.format($scope.item.place),
				content: "Кл. {0}, уч. {1}, ряд {2}, место {3}".format(
														$scope.cemetery.name, 
														$scope.area.name, 
														$scope.item.row || '-', 
														$scope.item.place),

				obj_type : 'place',
				id: $scope.item.id
			}];
    		$scope.placeCoordinates = [{
    			point : [
                         geo.getLat($scope.item.lat),
                         geo.getLng($scope.item.lng)
    			        ],
    			title : $scope.item.name,
    			obj_type : 'place',
    			id: $scope.item.id
    		}];
		}else{
		  ymapData.markers = [];
		}
		
		ymapData.points = [];
		angular.forEach($scope.graves, function(grave, key) {
			if (grave.lat && grave.lng) {
				ymapData.points.push({
					point : [
		                        geo.getLat(grave.lat || $scope.item.lat),
		                        geo.getLng(grave.lng || $scope.item.lng)
					         ],
					caption : 'Могила {0}'.format(grave.grave_number),
					content : '',
					obj_type : 'grave',
					id: grave.pk
				});
			}
		});
		$scope.$broadcast('handleMapChanged');
	};

	$scope.update = function() {
	    $scope.loading = true;
		item_params = {
			placeID : $routeParams.place_id,
			cemetery_id : $routeParams.cemetery_id,
			area_id : $routeParams.area_id,
			grave_page:$scope.grave_page,
			log_page:$scope.log_page
		};
		$scope.address_class = 'Place';
		$scope.address_class_params = item_params;


		Place.getForm(item_params, function(result) {
			
			$scope.cemetery = new Cemetery(result.cemetery);
			$scope.area = new Area(result.area);
			$scope.item = new Place(result.place);
			$scope.item.place_length = parseFloat($scope.item.place_length); // html5 input[type=number]
			$scope.item.place_width = parseFloat($scope.item.place_width);

			$scope.place_log = [];
			angular.forEach(result.log, function(item) {
                  $scope.place_log.push(new Log(item));
            });
			
			$scope.log_page = result.log_page;
			$scope.log_pages = result.log_pages; 
			
			$scope.responsible_phones = [];
			angular.forEach(result.responsible_phones, function(item) {
                  $scope.responsible_phones.push(new Phone(item));
            });

			//$scope.burials = new Burial(result.burials);
			//$scope.graves = new Grave(result.graves);
			
			if(result.responsible){
				$scope.responsible = new AlivePerson(result.responsible);
			}else{
				$scope.responsible = new AlivePerson({
					is_new : true
				});
			}

			$scope.item.name = $scope.cemetery.name;
			$scope.loading = false;
			
			$scope.grave_count = result.grave_count;
			$scope.updateGraves();
		},function(data){
		    $scope.loading = false;
			if(data.status==404){
				window.location = '/manage/404?title=Место не найдено';
			}
		});
		
	};

	$scope.updateGraves = function() {
	   $scope.loading = true;
	   
       item_params.grave_page = $scope.grave_page;
	   
	   Place.getGraves(item_params, function(graves) {
			//$scope.totalItems = graves.pages || 0;
			$scope.grave_page = graves.page || 1;
			$scope.grave_pages = graves.pages;
			

			//$scope.pageSize = 2;
                         
			delete $scope.graves;
			$scope.graves = [];
		    var grave;
			angular.forEach(graves.graves, function(row, key) {
				grave = new Grave(row);
				$scope.graves.push(grave);
				
				GravePhoto.query({
					grave_id : grave.id
				}, function(photos) {
					if (!_.isEmpty(photos)) {
						grave.photos = photos;
						grave.firstPhoto = _.first(photos).bfile;
					}
				});

			});
			delete $scope.burials;
            $scope.burials = [];
            var burial;
            angular.forEach(graves.burials, function(row, key) {
                burial = new Burial(row);
                $scope.burials.push(burial);
            });

			
			$scope.updateMap(); 
			$scope.loading = false;
            return;
		    
			// New element with default data
			$scope.newGrave = new Grave({
				place : $scope.item.id,
				grave_number : $scope.graves.length + 1, //todo add way to count next grave_number or add validation of grave_number
			});
		},function(data){
            $scope.loading = false;
        });
		/*Burial.query({
			cemetery_id : $routeParams.cemetery_id,
			area_id : $routeParams.area_id,
			place_id : $routeParams.place_id
		}, function(result) {
			$scope.burials = result;
		});*/
	};


    $scope.$watch("log_page", $scope.update);
    $scope.$watch("grave_page", $scope.updateGraves);    


	//alerts
	$scope.alerts = [];
	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};
	//$scope.alerts.push({msg: "Another alert!"});

	//todo extrude place and burial edit in separate controllers with tpl
	// Diallog
	$scope.opts = {
		backdropFade : true,
		dialogFade : true
	};

	$scope.openEditForm = function(form, data) {
		$scope[form] = true;
		$('body').css('overflow-y','hidden');
		switch (form) {
			case 'isBurialEditorOpen':
				if (data) {
					Burial.get({
						cemetery_id : $routeParams.cemetery_id,
						area_id : $routeParams.area_id,
						place_id : $routeParams.place_id,
						burialID : data.id
					}, function(result) {
						result.plan_time = result.plan_time || '';
						$scope.selectedBurial = result;
					});
				}
				break;
			case 'isGraveAddOpen':
				if ($scope.placeCoordinates) {
					var lat = $scope.placeCoordinates.lat, lng = $scope.placeCoordinates.lng;
				}
				$scope.newGrave = new Grave({
					place : $scope.item.id,
					grave_number : $scope.grave_count + 1, //todo add way to count next grave_number or add validation of grave_number
					lat :geo.getLat(lat || $scope.item.lat),
                    lng :geo.getLng(lng || $scope.item.lng )
				});
				break;
			case 'isGraveEditOpen':
				if (data) {
					$scope.selectedGrave = data;
					$scope.originGraveNumber = data.grave_number;
				}
				break;
			case 'isGraveGalleryOpen':
				if (data) {
					$scope.selectedGravePhotos = data;
					$scope.setCurrentImage(_.first(data));
				}
				break;
		}
	};

	$scope.setCurrentImage = function(image) {
		$scope.currentImage = image;
	};

	$scope.closeEditForm = function(form) {
		$scope[form] = false;
		$('body').css('overflow-y','auto');
		$scope.update();
	};

	//Place
	$scope.isPlaceEditorOpen = false;
	$scope.savePlaceEditForm = function(form) {
		if (form.$valid) {
			var url = '/manage/cemetery/{0}/area/{1}/place/{2}'.format($scope.item.cemetery, $scope.item.area, $scope.item.id);
			$scope.item.$update({
				cemetery_id : $routeParams.cemetery_id,
				area_id : $routeParams.area_id
			}, function() {
				$scope.update();
				$scope.updateMap();
				$scope.closeEditForm('isPlaceEditorOpen');
				noty({text: 'Изменения сохранены', type:'success', layout:'topRight'});
				$location.path(url);
				$location.replace();
				//$scope.update();
			});
		}
	};

	//Responsible
	$scope.isResponsibleEditorOpen = false;
	$scope.saveResponsibleEditForm = function(form) {
		if (form.$valid || true) { //TODO: check this
			$scope.item.obj_responsible = $scope.responsible;
			$scope.item.obj_responsible_phones = $scope.responsible_phones; 
			
			$scope.item.$update({
				cemetery_id : $routeParams.cemetery_id,
				area_id : $routeParams.area_id
			}, function() {
				noty({text: 'Изменения сохранены', type:'success', layout:'topRight'});
				$scope.update();
			});

			/*if ($scope.responsible.is_new) {
				$scope.responsible.$save(function(data) {
					$scope.item.responsible = data.id;
					$scope.item.$update({placeID : $routeParams.place_id,
										cemetery_id : $routeParams.cemetery_id,
										area_id : $routeParams.area_id
					},function() {
						noty({text: 'Изменения сохранены', type:'success', layout:'topRight'});
						$scope.update();
					});
				});
			} else {
				$scope.responsible.$update(function() {
					$scope.update();
					noty({text: 'Изменения сохранены', type:'success', layout:'topRight'});
				});
			}*/
			
			
			$scope.closeEditForm('isResponsibleEditorOpen');
		}else{
            var msg = "Исправьте ошибки в форме";
            noty({text: msg, type:'error', layout:'topRight'});
        }
	};

	$scope.removeResponsible = function() {
		if ($scope.responsible.last_name || $scope.responsible.first_name || $scope.responsible.middle_name) {
			var fio = "{0} {1} {2}".format($scope.responsible.last_name, $scope.responsible.first_name, $scope.responsible.middle_name);
			if (confirm("Открепить " + fio + '?')) {
				delete $scope.item.responsible;
				$scope.item.$update({placeID : $routeParams.place_id,
										cemetery_id : $routeParams.cemetery_id,
										area_id : $routeParams.area_id
					},function() {
					noty({text: fio + " откреплен.", type:'success', layout:'topRight'});
					$scope.update();
				});
			}
		}
	};

	$scope.onCemeteryChanged = function($item, $model) {
		if ($item) {
			Area.query({
				cemetery_id : $item.id
			}, function(result) {
				$scope.area_list = result;
			});

			$scope.area = undefined;
			$scope.cemetery = $item;
		}
	};

	$scope.onAreaChanged = function($item, $model, $value) {
		if ($item) {
			$scope.area = $item;
		}
	};

	//Grave
	
	$scope.graveMove = function(direction, grave){
		grave.$move({
				cemetery_id : $routeParams.cemetery_id,
				area_id : $routeParams.area_id,
				place_id : $routeParams.place_id,
				graveID:grave.id,
				direction:direction
			},function(data){
				$scope.update();
			});
	};

	$scope.isGraveAddOpen = false;

	$scope.addGrave = function(form) {
		if (form.$valid) {
			$scope.newGrave.$save(function() {
				$scope.closeEditForm('isGraveAddOpen');
				$scope.updateGraves();
				var msg = "Могила добавлена.";
				noty({text: msg, type:'success', layout:'topRight'});
				$scope.update();
			});
		}else{
            var msg = "Исправьте ошибки в форме";
            noty({text: msg, type:'error', layout:'topRight'});
		}
	};

	$scope.isGraveEditOpen = false;
	$scope.saveGraveEditForm = function(form) {
		if (form.$valid) {
			var graveUpdateHandler = function() {
				$scope.closeEditForm('isGraveEditOpen');
				$scope.updateGraves();
				var msg = "Изменения сохранены.";
				noty({text: msg, type:'success', layout:'topRight'});
			};

			var targetGrave = _.find($scope.graves, function(grave) {
				return grave.grave_number == $scope.selectedGrave.grave_number && grave.id !== $scope.selectedGrave.id;
			});

			if (targetGrave) {
				targetGrave.grave_number = $scope.originGraveNumber;
			}

			$scope.selectedGrave.$update(function(targetGrave) {
			    graveUpdateHandler();
			    //TODO: E.St.: is it necessary?
				/*if (targetGrave) {
					//targetGrave.$update(graveUpdateHandler);
				} else {
					graveUpdateHandler();
				}*/
				$scope.update();
			});
		}else{
            var msg = "Исправьте ошибки в форме";
            noty({text: msg, type:'error', layout:'topRight'});
        }
	};

	$scope.deleteGrave = function(grave) {
		
        var title = 'Подтверждение удаления',
            msg = 'Все прикрепленные фотографии также будут удалены',
            btns = [ {result:'ok', label: 'Удалить'},
            		 {result:'cancel', label: 'Отмена', cssClass: 'btn-primary'}];
        $scope.grave_to_delete = grave;
        $dialog.messageBox(title, msg, btns)
               .open()
               .then(function(result){
					if (result=='ok') {
						//var grave = new Grave($scope.grave_to_delete);
						$scope.grave_to_delete.$delete(function(){
							$scope.updateGraves();
							var msg = "Могила удалена.";
							noty({text: msg, type:'success', layout:'topRight'});
							delete $scope.grave_to_delete;
							$scope.update();
						});
					}
               });
	};

	$scope.haveBurials = function(graveID) {
		return _.any($scope.burials, function(burial) {
			return burial.grave === graveID;
		});
	};

	$scope.isGraveGalleryOpen = false;

	//Burial
	$scope.isBurialEditorOpen = false;
	$scope.saveBurialEditForm = function(form) {
		if (form.graveNumber.$dirty && form.$valid) {
			$scope.selectedBurial.grave_number = $scope.selectedBurial.grave.grave_number;
			$scope.selectedBurial.grave = $scope.selectedBurial.grave.id;
			$scope.selectedBurial.$update({
				cemetery_id : $routeParams.cemetery_id,
				area_id : $routeParams.area_id,
				place_id : $routeParams.place_id
			},function() {
				$scope.closeEditForm('isBurialEditorOpen');
				$scope.updateGraves();
				var msg = "Изменения сохранены.";
				noty({text: msg, type:'success', layout:'topRight'});
			});
		}
	};
	// EOF Diallog

	// RUN
	$scope.$on("$routeChangeSuccess", function(event) {
		$scope.update();
	});

	$scope.$on("mapPointChanged:place", function(event, data) {
		if($scope.item.id == data.obj_id){
			$scope.item.lat = data.coords[0];
			$scope.item.lng = data.coords[1];
			$scope.$digest();
			$scope.item.$update({
				cemetery_id : $routeParams.cemetery_id,
				area_id : $routeParams.area_id
			},function(data){
				$scope.update();
			});
		}
	});

	ymapData.markers = [];
	ymapData.points = [];
    $scope.$broadcast('handleMapChanged');

});
